﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using QJY.Common;
using QJY.Data;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web;

namespace QJY.API
{
    public class QYWDManage 
    {
    


        #region 我的收藏

        #region 获取我的收藏
        /// <summary>
        ///获取我的收藏
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void COLLECTLIST(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strWhere = "";
            strWhere = string.Format(" and MsgType ='{0}' order by CRDate desc", P1);
            if (P1 == "shortvideo")
            {
                strWhere = string.Format(" and MsgType in ('shortvideo','video') order by CRDate desc");
            }
            string strSql = string.Format("SELECT ID,MsgType,FromUserName,MediaId,ThumbMediaId,MsgId,AgentID,MsgContent,PicUrl,URL,Format,CRDate,Description,FileId,Title,Tags from  JH_Auth_WXMSG  where FromUserName='{0}' AND ComId={1} AND ModeCode='QYWD' {2}",  UserInfo.User.UserName, UserInfo.User.ComId, strWhere);
            msg.Result = new JH_Auth_WXMSGB().GetDTByCommand(strSql);
        }
        #endregion

        #region 我的收藏（修改获取笔记网页）
        /// <summary>
        /// 我的收藏添加修改
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void ADDTEXT(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            JH_Auth_WXMSG wxmsg = JsonConvert.DeserializeObject<JH_Auth_WXMSG>(P1);
            if (wxmsg.ID == 0)
            {
                wxmsg.ComId = UserInfo.User.ComId;
                wxmsg.FromUserName = UserInfo.User.UserName;
                wxmsg.CRDate = DateTime.Now;
                wxmsg.CRUser = UserInfo.User.UserName;
                wxmsg.MsgType = P2;
                wxmsg.ModeCode = "QYWD";
                new JH_Auth_WXMSGB().Insert(wxmsg);
            }
            else
            {
                new JH_Auth_WXMSGB().Update(wxmsg);
            }
            msg.Result = wxmsg;

        }


        /// <summary>
        /// 获取我的收藏byID
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1">ID</param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETTEXT(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int DataID = 0;
            int.TryParse(P1, out DataID);
            JH_Auth_WXMSG wxmsg = new JH_Auth_WXMSGB().GetEntity(d => d.ID == DataID);
            msg.Result = wxmsg;
        }
        #endregion

        #region 删除我的收藏byID
        public void DELSC(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            int[] scIDs = P1.SplitTOInt(',');
            new JH_Auth_WXMSGB().Delete(d => scIDs.Contains(d.ID));
        }

        #endregion

        /// <summary>
        /// 获取用户定义的标签
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void GETSCTAGS(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            List<string> Tags = new JH_Auth_WXMSGB().GetEntities(d => d.MsgType == P1 && d.CRUser == UserInfo.User.UserName && d.Tags != null && d.Tags != "" && d.ComId == UserInfo.User.ComId).Select(d => d.Tags).Distinct().ToList();
            msg.Result = Tags;
        }

        #endregion



        #region 接入Lotus云盘

        /// <summary>
        ///添加Lotus用户
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public void TBLOTUSUSER(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strCode = GETLOTUSUSERCODE(UserInfo.User.UserName, UserInfo.QYinfo.FileServerUrl, UserInfo.QYinfo.EncodingAESKey);
            string postUrl = string.Format(UserInfo.QYinfo.FileServerUrl + "/adminapi/ExeAction/TBUSER");
            Dictionary<String, String> DATA = new Dictionary<String, String>();
            DATA.Add("P1", P1);
            DATA.Add("filecode", strCode);
            HttpWebResponse ResponseData = CommonHelp.CreatePostHttpResponse(postUrl, DATA, 0, "", null);
            string strData = CommonHelp.GetResponseString(ResponseData);
            JObject JReturn = (JObject)JsonConvert.DeserializeObject(strData);
            //return JReturn["result"].ToString();
            msg.ErrorMsg = JReturn["ErrorMsg"].ToString(); 
        }


        public void GETLOTUSCODE(JObject context, Msg_Result msg, string P1, string P2, JH_Auth_UserB.UserInfo UserInfo)
        {
            string strCode = GETLOTUSUSERCODE(UserInfo.User.UserName, UserInfo.QYinfo.FileServerUrl, UserInfo.QYinfo.EncodingAESKey);
            msg.Result = strCode;
        }

        /// <summary>
        ///获取Lotus用户授权Code
        /// </summary>
        /// <param name="context"></param>
        /// <param name="msg"></param>
        /// <param name="P1"></param>
        /// <param name="P2"></param>
        /// <param name="UserInfo"></param>
        public string GETLOTUSUSERCODE(string strUser,string strFileServer, string strFileServerSecret)
        {
            string postUrl = string.Format(strFileServer + "/adminapi/getuser");
            Dictionary<String, String> DATA = new Dictionary<String, String>();
            DATA.Add("user", strUser);
            DATA.Add("Secret", strFileServerSecret);
            HttpWebResponse ResponseData = CommonHelp.CreatePostHttpResponse(postUrl, DATA, 0, "", null);
            string strSecret = CommonHelp.GetResponseString(ResponseData);
            JObject JReturn = (JObject)JsonConvert.DeserializeObject(strSecret);
            return JReturn["result"].ToString();
        }

        #endregion
    }
}