﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using QJY.Common;
using QJY.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace QJY.API
{




    











    #region 数据BI模块

    public class BI_DB_DimB : BaseEFDao<BI_DB_Dim> { }
    public class BI_DB_SetB : BaseEFDao<BI_DB_Set>
    {


        /// <summary>
        /// 获取Data得经纬度
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public List<BI_DB_Dim> getCType(DataTable dt)
        {

            List<BI_DB_Dim> ListDIM = new List<BI_DB_Dim>();
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                BI_DB_Dim DIM = new BI_DB_Dim();
                DIM.Name = dt.Columns[i].ColumnName;
                DIM.ColumnName = dt.Columns[i].ColumnName;
                DIM.ColumnSource = "0";
                string strType = dt.Columns[i].DataType.Name.ToLower();
                if (strType.Contains("int"))
                {
                    DIM.ColumnType = "Num";
                    DIM.Dimension = "2";
                }
                else if (strType.Contains("decimal") || strType.Contains("float") || strType.Contains("Double"))
                {
                    DIM.ColumnType = "float";
                    DIM.Dimension = "2";

                }
                else if (strType.Contains("datetime"))
                {
                    DIM.ColumnType = "Date";
                    DIM.Dimension = "1";
                }
                else
                {
                    DIM.ColumnType = "Str";
                    DIM.Dimension = "1";

                }
                ListDIM.Add(DIM);
            }
            return ListDIM;
        }
    }
    public class BI_DB_SourceB : BaseEFDao<BI_DB_Source>
    {

        /// <summary>
        /// 获取数据源
        /// </summary>
        /// <param name="intSID"></param>
        /// <returns></returns>
        public DBFactory GetDB(int intSID)
        {
            DBFactory db = new DBFactory();
            if (intSID == 0)//本地数据库
            {
                string strCon = new BI_DB_SetB().GetDBString();
                db = new DBFactory(strCon);
            }
            else
            {
                var tt = new BI_DB_SourceB().GetEntity(p => p.ID == intSID);
                if (tt != null)
                {
                    db = new DBFactory(tt.DBType, tt.DBIP, tt.Port, tt.DBName, tt.DBUser, tt.DBPwd);
                }
            }
            return db;

        }
    }
    public class BI_DB_YBPB : BaseEFDao<BI_DB_YBP>
    {


    }

    public class BI_DB_TableB : BaseEFDao<BI_DB_Table> { }


    #endregion



    


    



    
}
